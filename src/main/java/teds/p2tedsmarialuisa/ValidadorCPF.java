package teds.p2tedsmarialuisa;

public class ValidadorCPF {

	public boolean isValid(String cpf) {
		
		int soma, valor, digito, resultado, verificadorEsperado1, verificadorEsperado2;
		int digitoVerificador1, digitoVerificador2;
		
		if (cpf != null &&   //O CPF nao pode ser null
			!cpf.isBlank() &&   //O CPF nao pode estar vazio "" nem em branco "   "
			(cpf.length() == 11 || cpf.length() == 14)) { //O CPF deve ter o tamanho certo, com ou sem . e -
			
			//remove os caracteres especiais
			cpf = cpf.replaceAll("\\.", "");
			cpf = cpf.replaceAll("-", "");
			
			//pega os digitos verificadores do CPF
			digitoVerificador1 = Character.getNumericValue(cpf.charAt(9));
			digitoVerificador2 = Character.getNumericValue(cpf.charAt(10));
			
			//CPF: 123.456.789-09 12345678909
			//1º Dígito Verificador: 

			//Primeiro calculamos a Soma da multiplicação dos 9 primeiros dígitos por 10, 9, 8, ... , 3, 2, respectivamente. Ou seja:
			//Soma = (1*10) + (2*9) + (3*8) + (4*7) + (5*6) + (6*5) + (7*4) + (8*3) + (9*2) = 210
			soma = 0;
			for (int cont = 0; cont < 9; cont++) {
				digito = Character.getNumericValue(cpf.charAt(cont));
				soma += digito * (10 - cont);
			}
			
			//Em seguida, dividimos a Soma por 11 e multiplicamos o resultado por 11
			valor = (soma / 11) * 11;

			//Por fim, subtraímos Valor de Soma.
			resultado = soma - valor;

			//Agora analisamos Resultado:
			//Se Resultado for igual à 1 ou à 0, então o 1º dígito verificador é 0; 
			if (resultado == 1 || resultado == 0) {
				verificadorEsperado1 = 0;
			} else { //Caso contrário, o 1º dígito verificador é o resultado da subtração de Resultado de 11 (11 – Resultado).
				verificadorEsperado1 = 11 - resultado;
			}
			
			//2º Dígito Verificador: 
			//Primeiro calculamos a Soma da multiplicação dos 9 primeiros dígitos por 11, 10, 9, ..., 4, 3, respectivamente
			//e em seguida somamos com (Digito1 * 2), sendo que Digito1 é o valor encontrado para o 1º dígito verificador. Ou seja:
			//Soma = (1*11) + (2*10) + (3*9) + (4*8) + (5*7) + (6*6) + (7*5) + (8*4) + (9*3) + 0 * 2 = 255
			soma = 0;
			for (int cont = 0; cont < 9; cont++) {
				digito = Character.getNumericValue(cpf.charAt(cont));
				soma += digito * (11 - cont);
			}
			soma += digitoVerificador1 * 2;
			
			//O resto é semelhante ao que foi feito anteriormente.
			//Dividimos e multiplicamos por 11. 
			valor = (soma / 11) * 11;

			//Por fim, subtraímos Valor de Soma.
			resultado = soma - valor;

			//Agora analisamos Resultado: 
			//Se Resultado for igual à 1 ou à 0, então o 2º dígito verificador é 0; 
			if (resultado == 1 || resultado == 0) {
				verificadorEsperado2 = 0;
			} else { //Caso contrário, o 2º dígito verificador é o resultado da subtração de Resultado de 11 (11 – Resultado).
				verificadorEsperado2 = 11 - resultado;
			}
			
			//testa se os valores esperados batem com os do CPF
			return digitoVerificador1 == verificadorEsperado1 &&
				   digitoVerificador2 == verificadorEsperado2;
		} else {
			throw new IllegalArgumentException("Valor inválido de CPF");
		}
		
	}
	
}