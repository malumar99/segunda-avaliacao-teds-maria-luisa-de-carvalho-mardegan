package teds.p2tedsmarialuisa;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ValidadorCPFTest {

	@Test
	void testarCPFNull() {
		assertThrows(IllegalArgumentException.class, () -> {
			ValidadorCPF validatorCPF = new ValidadorCPF();
			validatorCPF.isValid(null);
		});
	}
	
	@Test
	void testarCPFBlank() {
		assertThrows(IllegalArgumentException.class, () -> {
			ValidadorCPF validatorCPF = new ValidadorCPF();
			validatorCPF.isValid("");
		});
	}
	
	@Test
	void testarTamanhoCPFIncorreto() {
		assertThrows(IllegalArgumentException.class, () -> {
			ValidadorCPF validatorCPF = new ValidadorCPF();
			String cpf10Digitos = "1234567890";
			validatorCPF.isValid(cpf10Digitos);
		});
	}

	
	@Test
	void testarVerificacaoCpfValido() {
		ValidadorCPF validatorCPF = new ValidadorCPF();
		String cpf = "888.127.559-76";
		
		assertTrue(validatorCPF.isValid(cpf));
	}
	
	@Test
	void testarVerificacaoCpfValidoSomenteNumeros() {
		ValidadorCPF validatorCPF = new ValidadorCPF();
		String cpf = "88812755976";
		
		assertTrue(validatorCPF.isValid(cpf));
	}
	
	@Test
	void testarVerificacaoCpfValidoPrimeiroDigito0() {
		//ESSE A SOMA DEVE SER 0 ou 1
		ValidadorCPF validatorCPF = new ValidadorCPF();
		String cpf = "207.087.141-02";
		
		assertTrue(validatorCPF.isValid(cpf));
	}
	

	@Test
	void testarVerificacaoCpfValidoSegundoDigito0() {
		ValidadorCPF validatorCPF = new ValidadorCPF();
		String cpf = "463.194.119-70";
		
		assertTrue(validatorCPF.isValid(cpf));
	}
	

	
	@Test
	void testarVerificacaoCpfInvalidoPrimeiroDigito() {
		//O PRIMEIRO DIGITO DESSE CPF DEVERIA SER 8 939.742.373-80
		ValidadorCPF validatorCPF = new ValidadorCPF();
		String cpf = "939.742.373-10";
		
		assertFalse(validatorCPF.isValid(cpf));
	}
	
	@Test
	void testarVerificacaoCpfInvalidoSegundoDigito() {
		//O SEGUNDO DIGITO DESSE CPF DEVERIA SER 0 939.742.373-80
		ValidadorCPF validatorCPF = new ValidadorCPF();
		String cpf = "939.742.373-81";
		
		assertFalse(validatorCPF.isValid(cpf));
	}
	
	
	/*
	 * DEPOIS DO PIT TEST - MATANDO MUTTATORS
	 */
	
	@Test
	void testarTamanhoCPFIncorreto2_Mutattor() {
		assertThrows(IllegalArgumentException.class, () -> {
			ValidadorCPF validatorCPF = new ValidadorCPF();
			String cpf13Digitos = "1234567890123";
			validatorCPF.isValid(cpf13Digitos);
		});
	}
	
	@Test
	void testarVerificacaoCpfValidoPrimeiroDigito0_Mutattor() {
		//ESSE A SOMA DEVE SER 0 ou 1
		ValidadorCPF validatorCPF = new ValidadorCPF();
		String cpf = "513.310.367-04";
		
		assertTrue(validatorCPF.isValid(cpf));
	}
	
	@Test
	void testarVerificacaoCpfValidoSegundoDigito0_Mutattor() {
		ValidadorCPF validatorCPF = new ValidadorCPF();
		String cpf = "939.742.373-80";
		
		assertTrue(validatorCPF.isValid(cpf));
	}
}
